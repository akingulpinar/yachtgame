#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Initialization of important functions
int Play_Computer(int first, int n, int userTotal, int computerTotal, int cat1, int cat2, int cat3, int cat4, int cat5, int cat6, int cat7, int cat8, int cat9, int cat10, int cat11, int cat12, int Usercat1, int Usercat2, int Usercat3, int Usercat4, int Usercat5, int Usercat6, int Usercat7, int Usercat8, int Usercat9, int Usercat10, int Usercat11, int Usercat12);
int Scoresheet(int category, int dice1, int dice2, int dice3, int dice4, int dice5);
int ComputerStrategyDecider(int cat1, int cat2, int cat3, int cat4, int cat5, int cat6, int cat7, int cat8, int cat9, int cat10, int cat11, int cat12, int dice1, int dice2, int dice3, int dice4, int dice5);

//The function for rolling a dice
int RollADice() {
	return 1 + rand() % 6;
}

//The function for calculation of ones
int CalcOnes(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == 1) sum++;
	if (dice2 == 1) sum++;
	if (dice3 == 1) sum++;
	if (dice4 == 1) sum++;
	if (dice5 == 1) sum++;
	return sum;
}

//The function for calculation of twos
int CalcTwos(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == 2) sum += 2;
	if (dice2 == 2) sum += 2;
	if (dice3 == 2) sum += 2;
	if (dice4 == 2) sum += 2;
	if (dice5 == 2) sum += 2;
	return sum;
}

//The function for calculation of threes
int CalcThrees(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == 3) sum += 3;
	if (dice2 == 3) sum += 3;
	if (dice3 == 3) sum += 3;
	if (dice4 == 3) sum += 3;
	if (dice5 == 3) sum += 3;
	return sum;
}

//The function for calculation of fours
int CalcFours(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == 4) sum += 4;
	if (dice2 == 4) sum += 4;
	if (dice3 == 4) sum += 4;
	if (dice4 == 4) sum += 4;
	if (dice5 == 4) sum += 4;
	return sum;
}

//The function for calculation of fives
int CalcFives(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == 5) sum += 5;
	if (dice2 == 5) sum += 5;
	if (dice3 == 5) sum += 5;
	if (dice4 == 5) sum += 5;
	if (dice5 == 5) sum += 5;
	return sum;
}

//The function for calculation of sixes
int CalcSixes(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == 6) sum += 6;
	if (dice2 == 6) sum += 6;
	if (dice3 == 6) sum += 6;
	if (dice4 == 6) sum += 6;
	if (dice5 == 6) sum += 6;
	return sum;
}

//The function for calculation of full house
int CalcFullHouse(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if ((dice1 == dice2&&dice1 == dice3&&dice4 == dice5) ||
		(dice1 == dice2&&dice1 == dice4&&dice3 == dice5) ||
		(dice1 == dice2&&dice1 == dice5&&dice3 == dice4) ||
		(dice1 == dice3&&dice1 == dice4&&dice2 == dice5) ||
		(dice1 == dice3&&dice1 == dice5&&dice2 == dice4) ||
		(dice2 == dice3&&dice2 == dice4&&dice1 == dice5) ||
		(dice2 == dice3&&dice2 == dice5&&dice1 == dice4) ||
		(dice2 == dice4&&dice2 == dice5&&dice1 == dice3) ||
		(dice3 == dice4&&dice3 == dice5&&dice1 == dice2) ||
		(dice1 == dice4&&dice1 == dice5&&dice2 == dice3)) {
		sum = dice1 + dice2 + dice3 + dice4 + dice5;
	}
	return sum;
}

//The function for calculation of four of a kind
int CalcFourOfAKind(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == dice2&&dice1 == dice3&&dice1 == dice4)sum = dice1 + dice2 + dice3 + dice4;
	else if (dice1 == dice2&&dice1 == dice3&&dice1 == dice5)sum = dice1 + dice2 + dice3 + dice5;
	else if (dice1 == dice2&&dice1 == dice4&&dice1 == dice5)sum = dice1 + dice2 + dice4 + dice5;
	else if (dice1 == dice3&&dice1 == dice4&&dice1 == dice5)sum = dice1 + dice3 + dice4 + dice5;
	else if (dice2 == dice3&&dice2 == dice4&&dice2 == dice5)sum = dice2 + dice3 + dice4 + dice5;
	return sum;
}

//The function for calculation of little straight
int CalcLittleStraight(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == 1 && dice2 == 2 && dice3 == 3 && dice4 == 4 && dice5 == 5)sum = 30;
	return sum;
}

//The function for calculation of big straight
int CalcBigStraight(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == 2 && dice2 == 3 && dice3 == 4 && dice4 == 5 && dice5 == 6)sum = 30;
	return sum;
}

//The function for calculation of choice
int CalcChoice(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	sum = dice1 + dice2 + dice3 + dice4 + dice5;
	return sum;
}

//The function for calculation of yacht
int CalcYacht(int dice1, int dice2, int dice3, int dice4, int dice5) {
	int sum = 0;
	if (dice1 == dice2 == dice3 == dice4 == dice5)sum = 50;
	return sum;
}

//The function responsible from human turns
//first variable is used whether the player starts the game or not,
//n is the round counter
//userTotal and computerTotal are for holding the total scores
//between cat1 and cat12 are the categories/rules status' of human 
//between PCcat1 and PCcat12 are the categories/rules status' of computer. They're used because of recursion  
int Play_Human(int first, int n, int userTotal, int computerTotal, int cat1, int cat2, int cat3, int cat4, int cat5, int cat6, int cat7, int cat8, int cat9, int cat10, int cat11, int cat12, int PCcat1, int PCcat2, int PCcat3, int PCcat4, int PCcat5, int PCcat6, int PCcat7, int PCcat8, int PCcat9, int PCcat10, int PCcat11, int PCcat12) {
	//dice1,dice2,dice3,dice4,dice5 are variables for dices
	//diceCounter is for checking the 3 times rule.
	//loopStatus is for breaking a loop
	//category is defined for choosing rules/categories
	//decision is for rolling again
	int dice1, dice2, dice3, dice4, dice5, diceCounter = 1, loopStatus = 1, category;
	char decision,c;

	if (n<13) {
		if (first == 1) {
			n++;//To fix the round counter
			if (n == 13) {//When 12 rounds are finished, totals will be shown
				printf("\nUser Total Score: %d\tComputer Total Score: %d\n", userTotal, computerTotal);
				//will return 1 or 0 to the main function in order to define the winner
				if (userTotal>computerTotal)return 1;
				else return 0;
			}
			printf("\nRound: %d\n======================================\n", n);
		}

		printf("Rolled the dice for you:\n");

		do {

			dice1 = RollADice(), dice2 = RollADice(), dice3 = RollADice(), dice4 = RollADice(), dice5 = RollADice();
			printf("Dice 1: %d\tDice 2: %d\tDice 3: %d\tDice 4: %d\tDice 5: %d\n", dice1, dice2, dice3, dice4, dice5);

			if (diceCounter == 3) {
				printf("\n");
				break;
			}

			//Allows human to dice up to three times
			do {			
				printf("\nDo you want to roll or not (Y/N):");
				scanf(" %c", &decision);
				if (decision != 'Y' && decision != 'y' && decision != 'N' &&decision != 'n')
					printf("Wrong option!");
				else
					diceCounter++;			
			} while (decision != 'Y' && decision != 'y' && decision != 'N' &&decision != 'n');
		} while (decision == 'Y' || decision == 'y');

		//Selection of rules and making their status 0;
		do {
			while ((c = getchar()) != '\n' && c != EOF);
			printf("Which scoring rule you would like to use (1: ones, 2: twos, 3: threes, 4: fours, 5: fives, 6: sixes, 7: full house, 8: four_of_a_kind, 9: little straight, 10: big straight, 11: choice, 12: yacht): ");
			scanf("%d", &category);
			//If the selected category's status equals to 1, then it will make it zero
			if (category == 1 && cat1 == 1) {
				cat1 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 2 && cat2 == 1) {
				cat2 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 3 && cat3 == 1) {
				cat3 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 4 && cat4 == 1) {
				cat4 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 5 && cat5 == 1) {
				cat5 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 6 && cat6 == 1) {
				cat6 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 7 && cat7 == 1) {
				cat7 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 8 && cat8 == 1) {
				cat8 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 9 && cat9 == 1) {
				cat9 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 10 && cat10 == 1) {
				cat10 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 11 && cat11 == 1) {
				cat11 = 0;
				loopStatus = 0;
				break;
			}
			else if (category == 12 && cat12 == 1) {
				cat12 = 0;
				loopStatus = 0;
				break;
			}
			else {
				if (category<1 || category>12) {
					printf("Wrong Option!\n");
				}
				else {
					printf("You've already chosen this option!\n");
				}
				loopStatus = 1;
				continue;
			}
		} while ((category<1 || category>12) || loopStatus == 1);
		//Calculates point and prints it
		int score = Scoresheet(category, dice1, dice2, dice3, dice4, dice5);
		userTotal += score;
		printf("Score: %d\tTotal Score: %d\n\n", score, userTotal);
		//Because of recursion, it will send variables to play_computer function
		Play_Computer(first, n, userTotal, computerTotal, PCcat1, PCcat2, PCcat3, PCcat4, PCcat5, PCcat6, PCcat7, PCcat8, PCcat9, PCcat10, PCcat11, PCcat12, cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8, cat9, cat10, cat11, cat12);
	}
}

//The function responsible from human turns
//first variable is used whether the computer starts the game or not,
//n is the round counter
//userTotal and computerTotal are for holding the total scores
//between cat1 and cat12 are the categories/rules status' of the computer 
//between PCcat1 and PCcat12 are the categories/rules status' of human. They're used because of recursion
int Play_Computer(int first, int n, int userTotal, int computerTotal, int cat1, int cat2, int cat3, int cat4, int cat5, int cat6, int cat7, int cat8, int cat9, int cat10, int cat11, int cat12, int Usercat1, int Usercat2, int Usercat3, int Usercat4, int Usercat5, int Usercat6, int Usercat7, int Usercat8, int Usercat9, int Usercat10, int Usercat11, int Usercat12) {
	int dice1, dice2, dice3, dice4, dice5, selectedCategory, score;
	char c;
	if (n<13) {
		if (first == 2) {
			n++;
			if (n == 13) {
				//Will show the total score at the end of the game.
				printf("\nUser Total Score: %d\tComputer Total Score: %d\n", userTotal, computerTotal);
				if (userTotal>computerTotal)return 1;
				else return 0;
			}
			printf("\nRound: %d\n======================================\n", n);
		}

		printf("Rolled the dice for the computer:\n");
		dice1 = RollADice(), dice2 = RollADice(), dice3 = RollADice(), dice4 = RollADice(), dice5 = RollADice();
		printf("Dice 1: %d\tDice 2: %d\tDice 3: %d\tDice 4: %d\tDice 5: %d\n", dice1, dice2, dice3, dice4, dice5);
		//Will send to computerStrategyDecider to find which rule gives the maximum points.
		selectedCategory = ComputerStrategyDecider(cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8, cat9, cat10, cat11, cat12, dice1, dice2, dice3, dice4, dice5);
		//Again, if the selected category's status equals to one, will make it zero.
		if (selectedCategory == 1 && cat1 == 1) {
			cat1 = 0;
		}
		else if (selectedCategory == 2 && cat2 == 1) {
			cat2 = 0;
		}
		else if (selectedCategory == 3 && cat3 == 1) {
			cat3 = 0;
		}
		else if (selectedCategory == 4 && cat4 == 1) {
			cat4 = 0;
		}
		else if (selectedCategory == 5 && cat5 == 1) {
			cat5 = 0;
		}
		else if (selectedCategory == 6 && cat6 == 1) {
			cat6 = 0;
		}
		else if (selectedCategory == 7 && cat7 == 1) {
			cat7 = 0;
		}
		else if (selectedCategory == 8 && cat8 == 1) {
			cat8 = 0;
		}
		else if (selectedCategory == 9 && cat9 == 1) {
			cat9 = 0;
		}
		else if (selectedCategory == 10 && cat10 == 1) {
			cat10 = 0;
		}
		else if (selectedCategory == 11 && cat11 == 1) {
			cat11 = 0;
		}
		else if (selectedCategory == 12 && cat12 == 1) {
			cat12 = 0;
		}
		//Calculates the score and prints it
		score = Scoresheet(selectedCategory, dice1, dice2, dice3, dice4, dice5);
		computerTotal += score;
		printf("Rule %d is used!\n", selectedCategory);
		printf("Score: %d\tTotal Score: %d\n\n", score, computerTotal);
		//Again will send the variables because of recursion to human.
		
		Play_Human(first, n, userTotal, computerTotal, Usercat1, Usercat2, Usercat3, Usercat4, Usercat5, Usercat6, Usercat7, Usercat8, Usercat9, Usercat10, Usercat11, Usercat12, cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8, cat9, cat10, cat11, cat12);
	}
}

int ComputerStrategyDecider(int cat1, int cat2, int cat3, int cat4, int cat5, int cat6, int cat7, int cat8, int cat9, int cat10, int cat11, int cat12, int dice1, int dice2, int dice3, int dice4, int dice5) {
	int SumOnes = 0, SumTwos = 0, SumThrees = 0, SumFours = 0, SumFives = 0, SumSixes = 0, SumFullHouse = 0, SumFourOfAKind = 0, SumLittleStraight = 0, SumBigStraight = 0, SumChoice = 0, SumYacht = 0, biggest = 0, selectedCategory;
	//If the status of a category is one, then it will calculate points from that category.
	if (cat1 == 1)SumOnes = CalcOnes(dice1, dice2, dice3, dice4, dice5);
	if (cat2 == 1)SumTwos = CalcTwos(dice1, dice2, dice3, dice4, dice5);
	if (cat3 == 1)SumThrees = CalcThrees(dice1, dice2, dice3, dice4, dice5);
	if (cat4 == 1)SumFours = CalcFours(dice1, dice2, dice3, dice4, dice5);
	if (cat5 == 1)SumFives = CalcFives(dice1, dice2, dice3, dice4, dice5);
	if (cat6 == 1)SumSixes = CalcSixes(dice1, dice2, dice3, dice4, dice5);
	if (cat7 == 1)SumFullHouse = CalcFullHouse(dice1, dice2, dice3, dice4, dice5);
	if (cat8 == 1)SumFourOfAKind = CalcFourOfAKind(dice1, dice2, dice3, dice4, dice5);
	if (cat9 == 1)SumLittleStraight = CalcLittleStraight(dice1, dice2, dice3, dice4, dice5);
	if (cat10 == 1)SumBigStraight = CalcBigStraight(dice1, dice2, dice3, dice4, dice5);
	if (cat11 == 1)SumChoice = CalcChoice(dice1, dice2, dice3, dice4, dice5);
	if (cat12 == 1)SumYacht = CalcYacht(dice1, dice2, dice3, dice4, dice5);

	//Get the points from the category which has a status of 1
	if (cat1 == 1) {
		biggest = SumOnes;
		selectedCategory = 1;
	}
	else if (cat2 == 1) {
		biggest = SumTwos;
		selectedCategory = 2;
	}
	else if (cat3 == 1) {
		biggest = SumThrees;
		selectedCategory = 3;
	}
	else if (cat4 == 1) {
		biggest = SumFours;
		selectedCategory = 4;
	}
	else if (cat5 == 1) {
		biggest = SumFives;
		selectedCategory = 5;
	}
	else if (cat6 == 1) {
		biggest = SumSixes;
		selectedCategory = 6;
	}
	else if (cat7 == 1) {
		biggest = SumFullHouse;
		selectedCategory = 7;
	}
	else if (cat8 == 1) {
		biggest = SumFourOfAKind;
		selectedCategory = 8;
	}
	else if (cat9 == 1) {
		biggest = SumLittleStraight;
		selectedCategory = 9;
	}
	else if (cat10 == 1) {
		biggest = SumBigStraight;
		selectedCategory = 10;
	}
	else if (cat11 == 1) {
		biggest = SumChoice;
		selectedCategory = 11;
	}
	else if (cat12 == 1) {
		biggest = SumYacht;
		selectedCategory = 12;
	}

	//It will check whether the category's point is the highest or not.If it is the biggest, it will choose that category
	if (SumTwos>biggest&& cat2 == 1) {
		biggest = SumTwos;
		selectedCategory = 2;
	}
	if (SumThrees>biggest&& cat3 == 1) {
		biggest = SumThrees;
		selectedCategory = 3;
	}
	if (SumFours>biggest&& cat4 == 1) {
		biggest = SumFours;
		selectedCategory = 4;
	}
	if (SumFives>biggest&& cat5 == 1) {
		biggest = SumFives;
		selectedCategory = 5;
	}
	if (SumSixes>biggest&& cat6 == 1) {
		biggest = SumSixes;
		selectedCategory = 6;
	}
	if (SumFullHouse>biggest&& cat7 == 1) {
		biggest = SumFullHouse;
		selectedCategory = 7;
	}
	if (SumFourOfAKind>biggest&& cat8 == 1) {
		biggest = SumFourOfAKind;
		selectedCategory = 8;
	}
	if (SumLittleStraight>biggest&& cat9 == 1) {
		biggest = SumLittleStraight;
		selectedCategory = 9;
	}
	if (SumBigStraight>biggest&& cat10 == 1) {
		biggest = SumBigStraight;
		selectedCategory = 10;
	}
	if (SumChoice>biggest&& cat11 == 1) {
		biggest = SumChoice;
		selectedCategory = 11;
	}
	if (SumYacht>biggest&& cat12 == 1) {
		biggest = SumYacht;
		selectedCategory = 12;
	}

	return selectedCategory;
}

int Scoresheet(int category, int dice1, int dice2, int dice3, int dice4, int dice5) {
	int score = 0;
	//It will calculate the point from the selected category.
	if (category == 1) score = CalcOnes(dice1, dice2, dice3, dice4, dice5);
	else if (category == 2) score = CalcTwos(dice1, dice2, dice3, dice4, dice5);
	else if (category == 3) score = CalcThrees(dice1, dice2, dice3, dice4, dice5);
	else if (category == 4) score = CalcFours(dice1, dice2, dice3, dice4, dice5);
	else if (category == 5) score = CalcFives(dice1, dice2, dice3, dice4, dice5);
	else if (category == 6) score = CalcSixes(dice1, dice2, dice3, dice4, dice5);
	else if (category == 7) score = CalcFullHouse(dice1, dice2, dice3, dice4, dice5);
	else if (category == 8) score = CalcFourOfAKind(dice1, dice2, dice3, dice4, dice5);
	else if (category == 9) score = CalcLittleStraight(dice1, dice2, dice3, dice4, dice5);
	else if (category == 10) score = CalcBigStraight(dice1, dice2, dice3, dice4, dice5);
	else if (category == 11) score = CalcChoice(dice1, dice2, dice3, dice4, dice5);
	else if (category == 12) score = CalcYacht(dice1, dice2, dice3, dice4, dice5);

	return score;
}

int main() {
	//dicePlayer and diceComputer is for the beginning, ie. , to find who is going to start.
	//first variable is for fixing the round counters in play_human and play_computer functions.
	//n is the round counter
	//winner will be either 1 or 0 to find who won.
	//Status variables is for hold the status of rules in order to find if the rule is used before or not.
	int dicePlayer, diceComputer, first = 1, userTotal = 0, computerTotal = 0, n = 0, winner = 0;
	int StatusOnes = 1, StatusTwos = 1, StatusThrees = 1, StatusFours = 1, StatusFives = 1, StatusSixes = 1, StatusFullHouse = 1, StatusFourOfAKind = 1, StatusLittleStraight = 1, StatusBigStraight = 1, StatusChoice = 1, StatusYacht = 1;
	int PCStatusOnes = 1, PCStatusTwos = 1, PCStatusThrees = 1, PCStatusFours = 1, PCStatusFives = 1, PCStatusSixes = 1, PCStatusFullHouse = 1, PCStatusFourOfAKind = 1, PCStatusLittleStraight = 1, PCStatusBigStraight = 1, PCStatusChoice = 1, PCStatusYacht = 1;

	//To dice randomly.
	srand(time(NULL));

	printf("Welcome to the Yacht game.\nLets see who is lucky!\n");
	//Will dice until one of the players dices bigger.
	do {
		dicePlayer = RollADice();
		diceComputer = RollADice();
		printf("Player: %d\tComputer: %d\n", dicePlayer, diceComputer);
		if (dicePlayer == diceComputer) {
			printf("Deuce! Rolling the dice again!\n");
		}
	} while (dicePlayer == diceComputer);
	//To display who is going to start the game.
	dicePlayer > diceComputer ? printf("Player") : printf("Computer");
	printf(" is the lucky one, lets get started!\n");
	//Will return 1 or 0 to define the winner.
	if (dicePlayer > diceComputer) {
		winner = Play_Human(first, n, userTotal, computerTotal, StatusOnes, StatusTwos, StatusThrees, StatusFours, StatusFives, StatusSixes, StatusFullHouse, StatusFourOfAKind, StatusLittleStraight, StatusBigStraight, StatusChoice, StatusYacht, PCStatusOnes, PCStatusTwos, PCStatusThrees, PCStatusFours, PCStatusFives, PCStatusSixes, PCStatusFullHouse, PCStatusFourOfAKind, PCStatusLittleStraight, PCStatusBigStraight, PCStatusChoice, PCStatusYacht);
	}
	else {
		first = 2;
		winner = Play_Computer(first, n, userTotal, computerTotal, PCStatusOnes, PCStatusTwos, PCStatusThrees, PCStatusFours, PCStatusFives, PCStatusSixes, PCStatusFullHouse, PCStatusFourOfAKind, PCStatusLittleStraight, PCStatusBigStraight, PCStatusChoice, PCStatusYacht, StatusOnes, StatusTwos, StatusThrees, StatusFours, StatusFives, StatusSixes, StatusFullHouse, StatusFourOfAKind, StatusLittleStraight, StatusBigStraight, StatusChoice, StatusYacht);
	}

	winner == 1 ? printf("User ") : printf("Computer ");
	printf("is the winner!\n");
	return 0;
}
